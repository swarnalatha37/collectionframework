package com.Collections;

import java.util.ArrayList;
import java.util.Collections;

public class ReverseElemntsInArrayList 
{

	public static void main(String[] args)
    {
		 ArrayList<String> arrList = new ArrayList<String>();
		 arrList.add(new String("Red"));
		  arrList.add(new String("Green"));
		 arrList.add(new String("Orange"));
		 arrList .add(new String("White"));
		 arrList.add(new String("Black"));
		  System.out.println("List before reversing :\n" +arrList);  
		  Collections.reverse(arrList);
		  System.out.println("List after reversing :\n" + arrList); 
	}
		
	}


