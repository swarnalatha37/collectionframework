package com.Collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class ComparatorDemo
{

	public static void main(String[] args) 
	{
		
		ArrayList<Student> arrList=new ArrayList();
		arrList.add(new Student(23,"Sunju",70.3f));
		arrList.add(new Student(25,"Anu",80.2f));
		arrList.add(new Student(20,"Mani",60.73f));

		System.out.println("Sorting by studntAge...");
		Collections.sort(arrList,new AgeComparator());
		Iterator itr=arrList.iterator();
		while(itr.hasNext())
		{
		   Student student=(Student)itr.next();
		   System.out.println(student.studntName+" "+student.studntAge+" "+student.studntPercentage);
		}

        System.out.println("sorting by studentName...");
       Collections.sort(arrList,new NameComparator());
       Iterator itr1=arrList.iterator();
       while(itr1.hasNext())
       {
       Student st=(Student)itr1.next();
       System.out.println(st.studntName+" "+st.studntAge+" "+st.studntPercentage);

     }
  }
}
		


